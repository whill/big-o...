/**
 * ...
 * Created by winfieldhill on 3/3/15.
 */

(function() {
    'use strict';
    var
        cache = {};

    this.tmpl = function tmpl(str, data) {
        var fn = !/[^\w]/.test(str) ?
            cache[str] = cache[str] ||
                tmpl(document.getElementById(str).innerHTML) :
            //
            // ...
            new Function
    };
}());
