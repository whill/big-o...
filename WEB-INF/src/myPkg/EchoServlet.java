package myPkg;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Override;
import java.lang.String;

public class EchoServlet extends HttpServlet {
    private static final String markupHttpDocTop = "" +
            "<!DOCTYPE html>" +
            "<html><head>" +
            "<meta http-equiv='Content-Type' content='text/html; charset='UTF-8'>" +
            "<title>Echo Form Response</title></head>";

    @Override
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

        ServletContext ctx = config.getServletContext();
        System.out.println(ctx.getAttribute("asd"));
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {
            out.println(markupHttpDocTop);
            out.println("<body><h2>You entered the following:</h2>");
            out.println("<div>Name: " + htmlFilter(request.getParameter("userName")) + "</div>");
            out.println("<div>Password: " + htmlFilter(request.getParameter("userPassword")) + "</div>");
            out.println("<div>Gender: " + htmlFilter(request.getParameter("userGender")) + "</div>");
            out.println("<div>Age: " + htmlFilter(request.getParameter("ageSelectbox")) + "</div>");
            out.println("<div>Languages: " + htmlFilter(request.getParameter("languageJavaCheckbox")) + "</div>");
            out.println("<div>Instruction: " + htmlFilter(request.getParameter("instructionArea")) + "</div>");
            out.println("</body></html>");
            out.close();
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }

    private static String htmlFilter(String userFieldInput) {
        if (userFieldInput == null) return null;
        int length = userFieldInput.length();
        StringBuilder result = new StringBuilder(length + 20);
        char aChar;

        for (int i = 0; i < length; i++) {
            aChar = userFieldInput.charAt(i);
            switch (aChar) {
                case '<': result.append("&lt;"); break;
                case '>': result.append("&gt;"); break;
                case '&': result.append("&amp;"); break;
                case '"': result.append("&quot;"); break;
                default: result.append(aChar);
            }
        }

        return result.toString();
    }
}