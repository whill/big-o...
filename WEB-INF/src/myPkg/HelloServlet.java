package myPkg;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Override;
import java.util.Enumeration;
import javax.servlet.*;
import javax.servlet.http.*;

public class HelloServlet extends HttpServlet {

    //Use wervlet init params to provide defaults and/or config values for this servlet instance.
    @Override
    public void init(ServletConfig config) throws ServletException
    {
        //Don't forget to initialize the parent class
        super.init(config);

        ServletContext context = config.getServletContext();
        context.setAttribute("defaultName", config.getInitParameter("defaultName"));
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
                throws IOException, ServletException
    {
        //set response msg MIME type
        response.setContentType("text/html;charset=UTF-8");
        //allocate an ouput writer to write the response msg to the network socket
        String iParam = getInitParameter("defaultName");

        ServletContext sc = getServletContext();
        Enumeration<String> scE = sc.getAttributeNames();
        System.out.println("hmm...." + sc.getAttribute("defaultName"));
        System.out.println("hmm...." + iParam);


        //write the response msg, in an html object
        try (PrintWriter out = response.getWriter())
        {
            out.println("<!DOCTYPE html");
            out.println("<html><head>");
            out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
            out.println("<title>Hello Servlet</title></head>");
            out.println("<body>");
            out.println("<h1>Hello, Servlet.</h1>");
            out.println("<p>Request URI: " + request.getRequestURI() + "</p>");
            out.println("<p>Protocol: " + request.getProtocol() + "</p>");
            out.println("<p>Path Info: " + request.getPathInfo() + "</p>");
            out.println("<p>Remote Address: " + request.getRemoteAddr() + "</p>");
            out.println("<p>A Random Number: <strong>" + Math.random() + "</strong></p>");
            out.println("</body>");
            out.println("</html>");
            out.close(); //Always close
        }
    }
}