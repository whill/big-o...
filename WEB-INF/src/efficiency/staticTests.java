package efficiency;

import java.lang.reflect.Array;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by winfieldhill on 2/27/15.
 * ...
 */
public class staticTests {

    public staticTests() {

    }

    /**
     * Returns the number of elements in a non-empty array that are greater than the mean of that Array
     *
     * @param a - an Array of doubles
     * @param mean - a double with the mean value to compare the values within the Array of doubles against
     * @return an int with the number of elements in "a" that are greater than the mean
     */
    public synchronized int algorithm1(double[] a, double mean) {
        AtomicInteger count = new AtomicInteger(0);

        for (double anA : a) {
            if (anA > mean) {
                count.getAndIncrement();
            }
        }

        return count.get();
    }

    public static void printMe() {
        System.out.println("-");
    }
    public void printMe2(int printThis) {
        System.out.println(printThis);
    }
    public static void main(String[] args) {
        staticTests.printMe();
        staticTests st = new staticTests();
        st.printMe2(st.algorithm1(new double[]{1.1, 2.3}, 0.5));
    }
}
