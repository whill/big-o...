package efficiency;

/**
 * Created by winfieldhill on 2/28/15.
 * ...
 */
public class BigO_1 {

    private static int count = 0;

    BigO_1() {

    }

    /**
     * averageMean has 4 internal assignment operations that occur once
     * the for each loop will iterate n + 1 times
     * The if will be evaluated n times
     *
     *
     * @param dbls - an array of doubles to be compared against a mean
     * @param mean - a double to be used for comparisons against an array of doubles
     * @return the tally (int) of occurrences when a value in the array is greater than the mean
     */
    public static synchronized int averageMean(double[] dbls, double mean) {
        for ( double dbl : dbls)
            if (dbl > mean)
                BigO_1.count += 1;

        return BigO_1.count;
    }

    public synchronized int[] cubeInput(int[] nums) {
        int[] powResults = new int[]{};
        int counter = 0;
        for (int ints : nums)
            powResults[counter] = (int)Math.pow(ints, 3);

        return powResults;
    }

    public static void main(String[] args) {
        BigO_1.averageMean(new double[]{1.0, 2.0}, 3.0);
        BigO_1 bo1 = new BigO_1();
        int[] inA = (new BigO_1()).cubeInput(new int[]{1,2,3});
        bo1.cubeInput(new int[]{3,2,1});
    }
}
