package efficiency;

/**
 * Created by winfieldhill on 3/3/15.
 * ...
 */
public class BigO_2 {
    public BigO_2() {}

    /**
     *
     * Worst case definition:
     * All but 1 double in []a will have a value of 1.0 and one will have a value of 0.0
     * SO: All but 1 in this case will be greater than the mean
     *
     * Calculating worstTime(n)
     * 1 > a
     * 1 > mean
     * 1 > n
     * 1 > count
     * 1 > i
     * 1 > return
     * ----
     * 6 statements to be executed
     *
     * n+1 > for loop comparison of i to n
     * n   > i incrementations
     * n   > if comparisons
     * n-1 > count incrementations
     * ----
     * worstTime(n) = 6 + (n+1) + n + n + (n-1) = 6 + 4n
     *
     *
     * 00000000000000000000000000000000
     *
     *
     * averageTime definition:
     * Half of the values in []a will be greater than the mean
     *
     * Calculating averageTime(n)
     * 1 > a
     * 1 > mean
     * 1 > n
     * 1 > count
     * 1 > i
     * 1 > return
     * ----
     * 6 statements to be executed
     *
     * n+1 > for loop comparison of i to n
     * n   > i incrementations
     * n   > if comparisons
     * n/2 > count incrementations
     * ----
     * averageTime(n) = 6 + (n+1) + n + n + (n/2) = 7 + 3.5n
     *
     *
     * 00000000000000000000000000000000
     *
     *
     * Since the []a is not created within aboveMeanCount it's space is limited to the other
     * indexed variables of: mean, n, count, i and the return assignment, so 5
     *
     * worstSpace(n) = averageSpace(n) = 5
     *
     *
     * 00000000000000000000000000000000
     *
     *
     * Big O:
     * f(n) <= C * g(n) n >= k
     *
     * C = 5, n = 10 -> 46 <  5 * 10
     * C = 1, n = 5  -> 26 !< 1 * 5
     * SO...
     * 6 + 4n = C * n -> 6/n + 4 = C so if:
     * n = 1 then C = 10 or if n = 6 then C = 5 so as n -> infinity: C > 4
     *
     * Test for each term in definition of f is <= to some constant * n (while n >= 0)
     * 6 <= 6 * n  - if n >= 0
     * 4n <= 4 * n - if n >= 1
     * So n = 1 so solving for C results in C = 10
     *
     * @param a - array of doubles
     * @param mean - double with the mean value being used for comparison sake
     * @return the count of occurences where a double in array a is gt mean
     */
    public static int aboveMeanCount(double[] a, double mean) {

        int n = a.length,
            count = 0;

        // Could be changed to: for ( double d : a ) ...
        for (int i = 0; i < n; i++) {
            if (a[i] > mean) {
                count++;
            }
        }

        return count;
    }

    public static void main(String[] args) {
        double[] da = {1.0, 2.0};

        BigO_2.aboveMeanCount(da, 1.5);
        BigO_2.aboveMeanCount(new double[]{1.0,1.0,1.0,1.0,0.0}, 0.80);
    }
}
